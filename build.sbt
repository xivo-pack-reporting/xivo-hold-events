import AssemblyKeys._

name := "xivo-hold-events"

version := "1.0"

libraryDependencies ++= Seq("com.typesafe.akka" %% "akka-actor" % "2.3.4",
                            "org.asteriskjava" % "asterisk-java" % "1.0.0.M3" exclude("javax.jms", "jms") exclude("com.sun.jdmk", "jmxtools") exclude("com.sun.jmx", "jmxri"),
                            "org.scalatest" % "scalatest_2.10" % "2.0" % "test",
                            "org.easymock" % "easymock" % "3.1" % "test",
                            "org.dbunit" % "dbunit" % "2.4.7" % "test",
                            "joda-time" % "joda-time" % "2.0",
                            "org.joda" % "joda-convert" % "1.2",
                            "postgresql" % "postgresql" % "9.1-901-1.jdbc4",
                            "ch.qos.logback" % "logback-classic" % "1.0.7",
                            "commons-configuration" % "commons-configuration" % "1.7")

assemblySettings

mergeStrategy in assembly <<= (mergeStrategy in assembly) {
  (old) => {
    case PathList("org", "apache", "commons", xs@_*) => MergeStrategy.first
    case x => old(x)
  }
}

jarName in assembly := "xivo-hold-events.jar"

test in assembly := {}
