DROP TABLE IF EXISTS hold_periods;
CREATE TABLE hold_periods (
    id SERIAL PRIMARY KEY,
    uniqueid VARCHAR(150),
    linkedid VARCHAR(150),
    start TIMESTAMP WITHOUT TIME ZONE,
    "end" TIMESTAMP WITHOUT TIME ZONE
);
