package xivo.holdevents

import org.asteriskjava.manager.event.{HangupEvent, DialEvent, HoldEvent}
import org.joda.time.format.DateTimeFormat
import org.scalatest.mock.EasyMockSugar
import org.scalatest.{BeforeAndAfterEach, FlatSpec, Matchers}

class HoldManagerSpec extends FlatSpec with Matchers with BeforeAndAfterEach with EasyMockSugar {

  var manager: HoldManager = null
  var dbWriter: DbWriter = null
  val format = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss")

  override def beforeEach() {
    dbWriter = mock[DbWriter]
    manager = new HoldManager(dbWriter)
  }

  "A HoldManager" should "add a new entry in the map and persist it when the hold starts" in {
    val date = format.parseDateTime("2014-01-01 08:00:00")
    val event = new HoldEvent()
    event.setStatus(true)
    event.setUniqueId("123456.789")
    event.setChannel("SIP/abcd-001")
    event.setDateReceived(date.toDate)
    dbWriter.persist(HoldPeriod("123456.789", format.parseDateTime("2014-01-01 08:00:00"), null))

    whenExecuting(dbWriter) {
      manager.onManagerEvent(event)
    }

    manager.map("SIP/abcd-001") shouldEqual HoldPeriod("123456.789", date, null)
  }

  it should "update the map when the hold ends and set the end date" in {
    val date = format.parseDateTime("2014-01-01 08:15:00")
    val event = new HoldEvent()
    event.setStatus(false)
    event.setUniqueId("123456.789")
    event.setChannel("SIP/abcd-001")
    event.setDateReceived(date.toDate)
    manager.map.put("SIP/abcd-001", HoldPeriod("123456.789", format.parseDateTime("2014-01-01 08:00:00"), null))
    dbWriter.setEndDate(HoldPeriod("123456.789", format.parseDateTime("2014-01-01 08:00:00"), date))

    whenExecuting(dbWriter) {
      manager.onManagerEvent(event)
    }
    manager.map.size shouldEqual 0
  }

  it should "update the map when the call is hanged up during the hold, and set the end date" in {
    val date = format.parseDateTime("2014-01-01 08:15:00")
    val event = new HangupEvent()
    event.setUniqueId("123456.789")
    event.setChannel("SIP/abcd-001")
    event.setDateReceived(date.toDate)
    manager.map.put("SIP/abcd-001", HoldPeriod("123456.789", format.parseDateTime("2014-01-01 08:00:00"), null))
    dbWriter.setEndDate(HoldPeriod("123456.789", format.parseDateTime("2014-01-01 08:00:00"), date))

    whenExecuting(dbWriter) {
      manager.onManagerEvent(event)
    }
    manager.map.size shouldEqual 0
  }

  it should "not fail when the hold ends and there is no hold period registered" in {
    val date = format.parseDateTime("2014-01-01 08:15:00")
    val event = new HoldEvent()
    event.setStatus(false)
    event.setChannel("SIP/abcd-001")
    event.setUniqueId("123456.789")
    event.setDateReceived(date.toDate)

    manager.onManagerEvent(event)
  }

  it should "not fail with another event" in {
    val event = new DialEvent()
    manager.onManagerEvent(event)
  }
}
