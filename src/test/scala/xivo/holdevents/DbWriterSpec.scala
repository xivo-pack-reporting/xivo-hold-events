package xivo.holdevents

import java.sql.Connection

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.postgresql.util.PSQLException
import org.scalatest.mock.EasyMockSugar
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, FlatSpec, Matchers}

import scala.collection.mutable

class DbWriterSpec extends FlatSpec with EasyMockSugar with BeforeAndAfterEach with BeforeAndAfterAll with Matchers {

  var factory: ConnectionFactory = null
  var dbWriter: DbWriter = null
  val format = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss")
  var connection: Connection = null

  override def beforeAll() {
    DBUtil.setupDB("holdperiods.xml")
  }

  override def beforeEach() {
    DBUtil.cleanTable("hold_periods")
    factory = new ConnectionFactory("localhost", "asterisktest", "asterisk", "asterisk")
    dbWriter = new DbWriter(factory)
    connection = factory.getConnection()
  }

  override def afterEach() {
    connection.close
  }

  "A DbWriter" should "write the HoldPeriod in the database" in {
    val period = HoldPeriod("123456.789", format.parseDateTime("2014-01-01 08:00:00"), null)

    dbWriter.persist(period)

    val stmt = connection.createStatement()
    val rs = stmt.executeQuery("SELECT uniqueid, start FROM hold_periods")
    rs.next() shouldBe true
    rs.getString("uniqueid") shouldEqual "123456.789"
    new DateTime(rs.getTimestamp("start")) shouldEqual format.parseDateTime("2014-01-01 08:00:00")
    rs.next() shouldBe false
  }

  it should "be able to reconnect" in {
    val mockFactory = mock[ConnectionFactory]
    val connection = mock[Connection]
    val connection2 = mock[Connection]
    mockFactory.getConnection().andReturn(connection)
    mockFactory.getConnection().andReturn(connection2)
    val l = mutable.MutableList(1)
    def failingFunction(a: Unit): Unit =
      if(l(0) == 1) {
        l(0) = 0
        throw new PSQLException("msg", null)
      } else {
        return
      }

    whenExecuting(mockFactory, connection, connection2) {
      val dbWriter = new DbWriter(mockFactory)
      dbWriter.withReconnection(failingFunction)
    }
  }

  it should "set the end date" in {
    val period = HoldPeriod("123456.789", format.parseDateTime("2014-01-01 08:00:00"), null)
    dbWriter.persist(period)
    period.end =  format.parseDateTime("2014-01-01 08:00:45")

    dbWriter.setEndDate(period)

    val stmt = connection.createStatement()
    val rs = stmt.executeQuery("SELECT uniqueid, start, \"end\" FROM hold_periods")
    rs.next() shouldBe true
    rs.getString("uniqueid") shouldEqual "123456.789"
    new DateTime(rs.getTimestamp("start")) shouldEqual format.parseDateTime("2014-01-01 08:00:00")
    new DateTime(rs.getTimestamp("end")) shouldEqual format.parseDateTime("2014-01-01 08:00:45")
    rs.next() shouldBe false
  }
}
