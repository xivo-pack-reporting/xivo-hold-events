package xivo.holdevents

import org.joda.time.DateTime
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, Matchers, FlatSpec}
import java.sql.{Timestamp, Connection}

class HoldEventsCompleterSpec extends FlatSpec with Matchers with BeforeAndAfterEach with BeforeAndAfterAll {

  val factory = new ConnectionFactory(DBUtil.HOST, DBUtil.DB_NAME, DBUtil.USER, DBUtil.PASSWORD)
  var connection: Connection = null

  override def beforeAll() {
    DBUtil.setupDB("holdperiods.xml")
  }

  override def beforeEach() {
    List("hold_periods", "cel").foreach(t => DBUtil.cleanTable(t))
    connection = factory.getConnection()
  }

  "A HoldEventsCompleter" should "set the linkedid of existing hold events using the CELs" in {
    val uniqueId1 = "123456.789"
    val linkedId1 = "123456.788"
    val uniqueId2 = "123456.789"
    val linkedId2 = "123456.788"
    val insertCel = connection.prepareStatement(
      """INSERT INTO cel(uniqueid, linkedid, eventtype, eventtime, userdeftype, cid_name, cid_num, cid_ani, cid_rdnis,
         cid_dnid, exten, context, channame, appname, appdata, amaflags, accountcode, peeraccount, userfield, peer)
         VALUES (?, ?, 'APP_START', '2014-07-10 00:00:00', '', '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '')""")
    insertCel.setString(1, uniqueId1)
    insertCel.setString(2, linkedId1)
    insertCel.executeUpdate()
    insertCel.setString(1, uniqueId2)
    insertCel.setString(2, linkedId2)
    insertCel.executeUpdate()
    insertCel.close()

    val insertHoldPeriods = connection.prepareStatement("INSERT INTO hold_periods(uniqueid, start, \"end\") VALUES (?, '2014-07-10 00:00:00', '2014-07-10 00:00:20')")
    insertHoldPeriods.setString(1, uniqueId1)
    insertHoldPeriods.executeUpdate()
    insertHoldPeriods.setString(1, uniqueId2)
    insertHoldPeriods.executeUpdate()
    insertHoldPeriods.close()

    val completer = new HoldEventsCompleter(connection)
    completer.completeHoldEvents()

    val getHoldPeriod = connection.prepareStatement("SELECT linkedid FROM hold_periods WHERE uniqueid = ?")
    getHoldPeriod.setString(1, uniqueId1)
    var res = getHoldPeriod.executeQuery()
    res.next()
    res.getString("linkedid") shouldEqual linkedId1

    getHoldPeriod.setString(1, uniqueId2)
    res = getHoldPeriod.executeQuery()
    res.next()
    res.getString("linkedid") shouldEqual linkedId2

    getHoldPeriod.close()
  }

  it should "remove stale hold periods" in {
    val (id1, id2, id3) = ("123456.777", "123456.888", "12456.999")
    val insertHoldPeriods = connection.prepareStatement("INSERT INTO hold_periods(uniqueid, start, \"end\") VALUES (?, ?, ?)")
    insertHoldPeriods.setString(1, id1)
    insertHoldPeriods.setTimestamp(2, new Timestamp(new DateTime().minusHours(1).getMillis))
    insertHoldPeriods.setTimestamp(3, null)
    insertHoldPeriods.executeUpdate()
    insertHoldPeriods.setString(1, id2)
    insertHoldPeriods.setTimestamp(2, new Timestamp(new DateTime().minusHours(1).getMillis))
    insertHoldPeriods.setTimestamp(3, new Timestamp(new DateTime().minusHours(1).plusMinutes(3).getMillis))
    insertHoldPeriods.executeUpdate()
    insertHoldPeriods.setString(1, id3)
    insertHoldPeriods.setTimestamp(2, new Timestamp(new DateTime().minusMinutes(3).getMillis))
    insertHoldPeriods.setTimestamp(3, null)
    insertHoldPeriods.executeUpdate()
    insertHoldPeriods.close()

    new HoldEventsCompleter(connection).cleanStalePeriods()

    val rs = connection.prepareStatement("SELECT uniqueid FROM hold_periods ORDER BY uniqueid ASC").executeQuery()
    rs.next() should be(true)
    rs.getString("uniqueid") shouldEqual id2
    rs.next() should be(true)
    rs.getString("uniqueid") shouldEqual id3
    rs.next should be(false)
  }
}
