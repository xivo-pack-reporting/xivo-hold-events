package xivo.holdevents

import java.io.File

import org.dbunit.database.DatabaseConnection
import org.dbunit.dataset.DefaultDataSet
import org.dbunit.dataset.csv.{CsvDataSet, CsvDataSetWriter}
import org.dbunit.dataset.xml.FlatXmlDataSet
import org.dbunit.operation.DatabaseOperation

object DBUtil {
  
  val HOST = "localhost"
  val DB_NAME = "asterisktest"
  val USER = "asterisk"
  val PASSWORD = "asterisk"
  
  val factory = new ConnectionFactory(HOST, DB_NAME, USER, PASSWORD)
  implicit val conn = factory.getConnection()
  private var dbunitConnection: DatabaseConnection = new DatabaseConnection(conn)

  def setupDB(filename: String) = {
    var dataset = new FlatXmlDataSet(getClass().getClassLoader().getResourceAsStream(filename))
    for (table <- dataset.getTableNames()) {
      val createTable = scala.io.Source.fromInputStream(getClass().getClassLoader().getResourceAsStream(s"${table}.sql")).mkString
      conn.createStatement().executeUpdate(createTable)
    }
    DatabaseOperation.CLEAN_INSERT.execute(dbunitConnection, dataset)
  }

  def createCsv(tableName: String) = {
    val table = dbunitConnection.createTable(tableName);
    val dataset = new DefaultDataSet(table)
    CsvDataSetWriter.write(dataset, new File(getClass().getClassLoader().getResource("CSV/").getFile()))
  }

  def insertFromCsv() = {
    var dataset = new CsvDataSet(new File(getClass().getClassLoader().getResource("CSV/").getFile()))
    DatabaseOperation.CLEAN_INSERT.execute(dbunitConnection, dataset)
  }
  
  def cleanTable(tableName: String) = conn.createStatement().executeUpdate(s"TRUNCATE TABLE $tableName CASCADE")

}