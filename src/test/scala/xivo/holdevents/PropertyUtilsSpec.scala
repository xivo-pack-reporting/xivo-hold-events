package xivo.holdevents

import java.util.{InvalidPropertiesFormatException, Properties}

import org.scalatest.{Matchers, FlatSpec}

class PropertyUtilsSpec extends FlatSpec with Matchers {

  "PropertyUtils" should "do nothing if all mandatory properties are present" in {
    val mandatoryProperties = List("prop1", "prop2", "prop3")
    val props = new Properties()
    props.put("prop1", "value1")
    props.put("prop2", "value2")
    props.put("prop3", "value3")

    PropertyUtils.checkProperties(props, mandatoryProperties)
  }

  it should "throw an exception if one is missing" in {
    val mandatoryProperties = List("prop1", "prop2", "prop3")
    val props = new Properties()
    props.put("prop1", "value1")
    props.put("prop2", "value2")

    intercept[InvalidPropertiesFormatException] {
      PropertyUtils.checkProperties(props, mandatoryProperties)
    }
  }
}
