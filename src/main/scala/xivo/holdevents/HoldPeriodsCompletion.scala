package xivo.holdevents

import org.apache.commons.configuration.PropertiesConfiguration
import org.apache.log4j.Logger
import org.slf4j.LoggerFactory

object HoldPeriodsCompletion {

  val ConfigFile = "/etc/xivo-hold-events/xivo-hold-events.properties"
  val logger = LoggerFactory.getLogger(getClass)
  Logger.getRootLogger().setLevel(org.apache.log4j.Level.OFF)

  def main(args: Array[String]) {
    logger.info("Starting xivo-hold-events-completion")
    val config = new PropertiesConfiguration(ConfigFile)
    val fac = new ConnectionFactory(config.getString("reporting.address"),
                                    config.getString("reporting.database.name"),
                                    config.getString("reporting.database.user"),
                                    config.getString("reporting.database.password"))
    val conn = fac.getConnection()
    val completer = new HoldEventsCompleter(conn)
    logger.info("Cleaning stale hold periods")
    completer.cleanStalePeriods()
    logger.info("Positioning linkedid for hold periods")
    completer.completeHoldEvents()
  }
}
