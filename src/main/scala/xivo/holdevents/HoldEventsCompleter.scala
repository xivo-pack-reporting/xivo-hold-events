package xivo.holdevents

import java.sql.Connection

class HoldEventsCompleter(connection: Connection) {

  def completeHoldEvents(): Unit = {
    val updateStmt = connection.prepareStatement("UPDATE hold_periods SET linkedid = ? WHERE uniqueid = ?")
    val stmt = connection.prepareStatement("SELECT DISTINCT uniqueid FROM hold_periods WHERE linkedid IS NULL")
    val rs = stmt.executeQuery()
    while(rs.next()) {
      val uniqueid = rs.getString("uniqueid")
      linkedIdFromUniqueId(uniqueid).foreach(linkedId => {
        updateStmt.setString(1, linkedId)
        updateStmt.setString(2, uniqueid)
        updateStmt.executeUpdate()
      })
    }
    stmt.close()
    updateStmt.close()
  }

  def linkedIdFromUniqueId(uniqueId: String) = {
    var res: Option[String] = None
    val stmt = connection.prepareStatement("SELECT linkedid FROM cel WHERE uniqueid = ? FETCH FIRST ROW ONLY")
    stmt.setString(1, uniqueId)
    val rs = stmt.executeQuery()
    if(rs.next()) {
      res = Some(rs.getString("linkedId"))
    }
    stmt.close
    res
  }

  def cleanStalePeriods(): Unit = {
    val stmt = connection.prepareStatement("DELETE FROM hold_periods WHERE \"end\" IS NULL AND start < now() - INTERVAL '30 minutes'")
    stmt.executeUpdate()
    stmt.close
  }

}
