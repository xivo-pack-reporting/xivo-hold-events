package xivo.holdevents

import java.util.{InvalidPropertiesFormatException, Properties}

object PropertyUtils {
  def checkProperties(props: Properties, requiredProps: List[String]) {
    requiredProps match {
      case List() =>
      case h::q => {
        if(props.containsKey(h))
          checkProperties(props, q)
        else
          throw new InvalidPropertiesFormatException(s"Missing mandatory property $h")
      }
    }
  }
}
