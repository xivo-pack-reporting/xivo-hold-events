package xivo.holdevents

import org.apache.commons.configuration.PropertiesConfiguration
import org.asteriskjava.manager.{ManagerConnectionFactory, ManagerEventListenerProxy}
import org.slf4j.LoggerFactory

object HoldEventsDaemon {

  val ConfigFile = "/etc/xivo-hold-events/xivo-hold-events.properties"
  val logger = LoggerFactory.getLogger(getClass)

  def main(args: Array[String]) = {
    logger.info("Starting xivo-hold-events")
    val config = new PropertiesConfiguration(ConfigFile)
    val fac = new ManagerConnectionFactory(config.getString("xivo.address"),
                                           config.getString("xivo.ami.user"),
                                           config.getString("xivo.ami.password"))
    val conn = fac.createManagerConnection()
    val dbWriter = new DbWriter(new ConnectionFactory(config.getString("reporting.address"),
                                                      config.getString("reporting.database.name"),
                                                      config.getString("reporting.database.user"),
                                                      config.getString("reporting.database.password")))
    conn.addEventListener(new ManagerEventListenerProxy(new HoldManager(dbWriter)))
    conn.login("call")
    synchronized(wait())
    logger.info("Stopping xivo-hold-events")
  }

}
