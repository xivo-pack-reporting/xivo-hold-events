package xivo.holdevents

import java.sql.Timestamp

import org.postgresql.util.PSQLException
import org.slf4j.LoggerFactory

class DbWriter(factory: ConnectionFactory) {

  val logger = LoggerFactory.getLogger(getClass)
  var connection = factory.getConnection()

  def persist(holdPeriod: HoldPeriod): Unit =
    withReconnection(Unit => {
      val stmt = connection.prepareStatement("INSERT INTO hold_periods(uniqueid, start) VALUES (?, ?)")
      stmt.setString(1, holdPeriod.uniqueId)
      stmt.setTimestamp(2, new Timestamp(holdPeriod.start.toDate.getTime))
      stmt.executeUpdate()
      return
    })

  def setEndDate(holdPeriod: HoldPeriod): Unit = withReconnection(Unit => {
      val stmt = connection.prepareStatement("UPDATE hold_periods SET \"end\" = ? WHERE uniqueid = ? and start = ?")
      stmt.setTimestamp(1, new Timestamp(holdPeriod.end.toDate.getTime))
      stmt.setString(2, holdPeriod.uniqueId)
      stmt.setTimestamp(3, new Timestamp(holdPeriod.start.toDate.getTime))
      stmt.executeUpdate()
      return
    })

  def withReconnection(f: Unit => Unit): Unit = try {
    f()
  }  catch {
    case e: PSQLException => {
      logger.warn("Disconnected from database, reconnecting...", e)
      Thread.sleep(100)
      connection = factory.getConnection()
      f()
    }
    case e: Exception => {
      logger.error("Error writing to the database", e)
    }
  }
}
