package xivo.holdevents

import org.joda.time.DateTime

case class HoldPeriod(uniqueId: String, start: DateTime, var end: DateTime)
