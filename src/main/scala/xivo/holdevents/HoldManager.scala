package xivo.holdevents

import org.asteriskjava.manager.ManagerEventListener
import org.asteriskjava.manager.event.{HangupEvent, HoldEvent, ManagerEvent}
import org.joda.time.DateTime
import org.slf4j.LoggerFactory

import scala.collection.mutable.Map

class HoldManager(dbWriter: DbWriter) extends ManagerEventListener {
  val logger = LoggerFactory.getLogger(getClass)
  val map = Map[String, HoldPeriod]()

  override def onManagerEvent(event: ManagerEvent) = event match {
    case e: HoldEvent =>  processEvent(e)
    case e: HangupEvent => processEvent(e)
    case e: ManagerEvent =>
  }

  private def processEvent(event: HoldEvent) = {
    if(event.isHold) {
      logger.info(s"Hold started on channel ${event.getChannel}")
      val period = HoldPeriod(event.getUniqueId, new DateTime(event.getDateReceived), null)
      map.put(event.getChannel, period)
      dbWriter.persist(period)
    }
    else
      processEnd(event.getChannel, event)
  }

  private def processEvent(event: HangupEvent) = processEnd(event.getChannel, event)

  private def processEnd(channel: String, event: ManagerEvent) =  map.remove(channel).foreach(
    holdPeriod => {
      logger.info(s"Hold stoppped on channel $channel")
      holdPeriod.end = new DateTime(event.getDateReceived)
      dbWriter.setEndDate(holdPeriod)
    })
}
